# encoding: utf-8
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from getpass import getpass
import os
import time

def op_invalida():
    header()
    print("Escolha uma opção válida!")
    time.sleep(3)

def header():
    os.system("clear")
    print("{:=<49}".format(""))
    print("{:=^49}".format(" Comunicação Segura em Situações de Emergência "))
    print("{:=<49}".format("")+ "\n")

def psswd():
    while True:
        header()
        pwd = getpass("Digite a senha:")
        if len(pwd) == 0:
            header()
            print("A senha não pode estar vazia! Tente novamente!")
            time.sleep(3)
        else:
            pwd_c = getpass("\nDigite novamente a senha:")
            if pwd == pwd_c:
                break
            else:
                header()
                print("As senhas são diferentes. Tente novamente!")
                time.sleep(3)
    pwd_list = []
    new_hash = SHA256.new() # Bloco segredo, gera o IV com a hash sha256 da senha e a senha final com a hash sha256 do IV
    new_hash.update(pwd.encode("utf-8"))
    iv_final = new_hash.hexdigest()
    new_hash = SHA256.new()
    new_hash.update(iv_final.encode("utf-8"))
    pwd_final = new_hash.hexdigest()
    pwd_list.append(pwd_final)
    pwd_list.append(iv_final)
    return pwd_list

def crypt():
    while True:
        header()
        msg = input("Digite a mensagem\n(máximo 128 caracteres)\n\n")
        if len(msg.encode("utf-8")) > 128:
            header()
            print("A mensagem excede 128 caracteres. Tente novamente!")
            print("Atenção! Acentos contam como caracteres.")
            time.sleep(5)
        elif len(msg) == 0:
            header()
            print("Mensagem vazia. Tente novamente!")
            time.sleep(5)
        else: 
            while len(msg.encode("utf-8")) < 128:
                msg = msg + " "
            break
    pwd = psswd()
    obj = AES.new(pwd[0][:32], AES.MODE_CBC, pwd[1][:16])
    cifra = obj.encrypt(msg)
    cifra_final = cifra
    nome_arquivo = str(time.time())
    arquivo = open(nome_arquivo, "wb")
    arquivo.write(cifra_final)
    arquivo.close()
    header()
    input("Arquivo exportado como "+ nome_arquivo + "\n\n(Pressione enter para voltar ao menu!)")

def decrypt():
    header()
    nome_arquivo = input("Insira o nome do arquivo\n(Deve estar na pasta do main.py)\n\n>> ")
    try:
        with open(nome_arquivo, 'rb') as arquivo:
            cifra = arquivo.read()
    except FileNotFoundError:
        print("\n\nO arquivo \'"+ nome_arquivo+ "\' não existe. Tente novamente!")
        time.sleep(5)
        return
    pwd = psswd()
    obj = AES.new(pwd[0][:32], AES.MODE_CBC, pwd[1][:16])
    try:
        msg_final = obj.decrypt(cifra)
    except ValueError:
        header()
        print("O arquivo provavelmente está corrompido.\nImpossivel descriptografia da mensagem!")
        time.sleep(5)
        return
    header()
    print("Mensagem descrpitografada: \n")
    try:
        input(str(msg_final.decode("utf-8"))+"\n\n(Pressione enter para sair!)")
    except UnicodeDecodeError:
        print("\nSenha de descriptografia incorreta!")
        time.sleep(5)


lista_sim = ['s', 'sim', 'S', 'Sim', 'SIM', 'y', 'yes', 'Y', 'YES']
while True:
    header()
    print("Selecione uma opção:\n\n1. Criptografar Mensagem\n2. Descriptografar Mensagem\n0. Sair\n")
    choice = input(">> ")
    if len(choice) != 1:
        op_invalida()
    elif choice == '0':
        break
    elif choice == '1':
        crypt()
    elif choice == '2':
        decrypt()
    else:
        op_invalida()
